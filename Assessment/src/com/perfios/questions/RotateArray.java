package com.perfios.questions;

import java.util.Scanner;

public class RotateArray {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the size of array.");
		int n = sc.nextInt();
		int arr[] = new int[n];
		System.out.println("Enter Elements");
		for(int i=0;i<n;i++)
		{
			arr[i] = sc.nextInt();
		}
		System.out.println("Enter number of rotations");
		int rotations  = sc.nextInt();
		System.out.println("Original Array: ");
		for(int i = 0; i<n; i++)
		{
			System.out.print(arr[i] + " ");
		}
		rotate(arr, rotations, n);
		System.out.println("Rotated Array: ");
		for(int i = 0; i<n; i++)
		{
			System.out.print(arr[i] + " ");
		}
		sc.close();
	}

	public  static void rotate(int arr[], int rotations, int n)
	{
		int requiredRotations = rotations%n;
		for(int i = 0; i< requiredRotations ; i++)
		{
			int extra = arr[n-1];
			for(int j = n-1; j>0 ; j--)
			{
				arr[j] = arr[j-1];
			}
			arr[0] = extra;
		}
	}
}












