package com.perfios.questions;

import java.util.Scanner;

public class SumTarget {
	public static void main(String args[])
	{
		System.out.println("enter size of array");
		Scanner sc=new Scanner(System.in);
		int sz=sc.nextInt();
		int a[]=new int[sz];
		int ind1=0,ind2=0,found=0;
		System.out.println("enter elements");
		for(int i=0;i<sz;i++)
		{
			a[i]=sc.nextInt();
		}
		System.out.println("enter target");
		int target=sc.nextInt();
		for(int i=0;i<sz;i++)
		{
			for(int j=0;j<sz;j++)
			{
				if(a[i]+a[j]==target && j!=i)
				{
					ind1=i;
					ind2=j;
					found=1;
					break;
				}
			}
		}
		if(found==1)
		{
		System.out.println("indices are"+ind1+","+ind2);
		}
		else {
			System.out.println("none found");
		}
	}
}
