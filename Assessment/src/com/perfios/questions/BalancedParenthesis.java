package com.perfios.questions;

import java.util.Scanner;

public class BalancedParenthesis {
	public static void main(String args[])
	{
		String expr;
		Scanner sc=new Scanner(System.in);
		System.out.println("enter expression");
		expr=sc.next();
		int length=expr.length();
		int i=0;
		int copen=0;
		int cclosed=0;
		while(i<length)
		{
			if(expr.charAt(i)=='(')
			{
				copen++;
			}
			if(expr.charAt(i)==')')
			{
				cclosed++;
			}
			i++;
		}
		if(copen==cclosed)
		{
			System.out.println("Balanced");
		}
		else {
			System.out.println("Not Balanced");
		}
	}
}
