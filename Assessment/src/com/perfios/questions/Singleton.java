package com.perfios.questions;

class Stud {
		static Stud obj=null;
		private Stud()
		{
			System.out.println("Default constructor");
		}
		public static Stud getObj()
		{
			if(obj==null)
			{
				obj=new Stud();
			}
			return obj;
		}
	}
	public class Singleton {
		public static void main(String args[])
		{
			Stud obj=Stud.getObj();
			Stud obj2=Stud.getObj();
			System.out.println(obj.hashCode());
			System.out.println(obj2.hashCode());
		}
	}
